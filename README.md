# ts-config-bundler

config bundler

```
config/
  config.production.js
  config.test.js
  config.js
```

```javascript
const gulp = require("gulp"),
    configBundler = require("@stembord/config-bundler");

gulp.task("config", () =>
    gulp.src([
            path.join("config/config.*.js")),
            path.join("config/config.js"))
        ])
        .pipe(configBundler({
            options: {
                port: 8080
            }
        }))
        .pipe(gulp.dest("app/js"))
);
```
