import fs = require("fs-extra");
import gulp = require("gulp");
import path = require("path");
import tape = require("tape");
import through = require("through2");
import { configBundler } from "./configBundler";

const bundle = (out: string, callback: () => void) => {
  const dist = path.join(__dirname, out);

  if (fs.existsSync(dist)) {
    fs.removeSync(dist);
  }

  gulp
    .src([path.join(__dirname, "__test__", "config", path.join("*.js"))])
    .pipe(
      configBundler({
        options: {
          port: 8080
        }
      })
    )
    .pipe(gulp.dest(dist))
    .pipe(
      through.obj(
        (chunk, enc, callback) => {
          callback(undefined, chunk);
        },
        () => {
          callback();
        }
      )
    );
};

tape("config bundler", (assert: tape.Test) => {
  bundle(path.join("__test__", "dist"), () => {
    assert.equals(
      fs.existsSync(path.join(__dirname, "__test__", "dist", "config.json")),
      true,
      "config.json should exists"
    );
    assert.deepEquals(
      JSON.parse(
        fs
          .readFileSync(path.join(__dirname, "__test__", "dist", "config.json"))
          .toString()
      ),
      {
        url: "http://dev.example.com:8080",
        origUrl: "http://example.com:8080",
        dev: true,
        stay: "Stay"
      }
    );
    assert.end();
  });
});
