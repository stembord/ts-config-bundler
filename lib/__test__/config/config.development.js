module.exports = function(options, config) {
  return {
    origUrl: config.url,
    url: "http://dev.example.com:" + options.port,
    dev: true
  };
};
