import path = require("path");
import through = require("through2");
import File = require("vinyl");

const reload = (path: string) => {
  delete require.cache[require.resolve(path)];
  return module.require(path);
};

export interface IOptions<T> {
  options?: T;
  fileName?: string;
}

const getDefaultsDefault = <T, U>(value: T): U => value as any;
const getEnvironmentsDefault = <T, U>(options: T, config: U) => ({});

export const configBundler = <T, U>(options: IOptions<T> = {}) => {
  const env = process.env.NODE_ENV || "development",
    reEnv = new RegExp("\\b" + env + "\\b");

  let getDefaults = getDefaultsDefault,
    getEnvironments = getEnvironmentsDefault;

  options.options = options.options || ({} as any);
  options.fileName = path.basename(options.fileName || "config", ".js");

  const reName = new RegExp("\\b" + options.fileName + "\\b");
  const reNameWithExt = new RegExp("\\b" + options.fileName + "\\.js\\b");

  return through.obj(
    (file, enc, callback) => {
      const basename = path.basename(file.path);

      if (
        reName.test(basename) &&
        reEnv.test(basename) &&
        !reNameWithExt.test(basename)
      ) {
        if (getEnvironments === getEnvironmentsDefault) {
          getEnvironments = reload(file.path);
        }
      } else if (reNameWithExt.test(basename)) {
        if (getDefaults === getDefaultsDefault) {
          getDefaults = reload(file.path);
        }
      }

      callback();
    },
    function(callback) {
      const config = getDefaults(options.options as T),
        environments = getEnvironments(options.options as T, config),
        mergedConfig = { ...config, ...environments };

      this.push(
        new File({
          path: options.fileName + ".json",
          contents: Buffer.from(JSON.stringify(mergedConfig, null, 2))
        })
      );

      callback();
    }
  );
};
